Greenwave Medical Marihuana Dispensary (also known as Provisioning Center) is conveniently located at the cross streets of Cedar and Oakland in downtown Lansing, Michigan. Our mission is to help patients get into the cannabis industry while bringing FDA standards to dispensing medical marijuana. We focus on quality assurance and patient care and strive to educate patients on cannabinoid therapy and strains that target illness and condition alleviation. Every batch of medicine we carry is tested by an ISO Certified laboratory. All dispensary agents in our downtown Michigan dispensary location are highly trained in patient care from the most accredited cannabis training institution in the country.

If you are a patient looking for a different and discreet patient experience where the patient care specialists (also know as budtenders or dispensary agents) understand cannabis strains and their therapeutic benefits, please make an appointment with one of our Lansing, Michigan certified agents to aid in your understanding of the cannabis plant.

Address : 500 E Oakland Ave, Lansing, Michigan 48906, USA

Phone : 517-763-2717
